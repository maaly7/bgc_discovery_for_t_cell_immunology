## **BGC DISCOVERY FOR T-CELL IMMUNOLOGY** 

- This repository was developed for the [EMBL-EBI Bioinformatics for T-Cell immunology course](https://www.ebi.ac.uk/training/events/bioinformatics-t-cell-immunology-2022/) to train participants on discovering o-antigen BGCs in microbiome whole genome shotgun sequences.   

- This repository offers BGC and saccharide-BGC classifiers that identifies BGCs in whole genome sequences as well as classify them into saccharide or other BGC. Furthermore, the repository offers training datasets to train additional classifiers to identify other types of BGCs, such as those producing antibiotics, antiviral, antibacterial, antitumor and antigens. 

#### **System Requirements**

- This repository requires a computer with `RAM: 16GB`, `CPU: 6vCore`, `SSD: 30GB`. This version has been tested on Linux operating system and [Google Colab](https://colab.research.google.com) .

#### **Installation**
  
  - To clone bgcs_discovery repository:
  
    ```
    $ git clone https://gitlab.com/maaly7/bgc_discovery_for_t_cell_immunology.git
    $ cd bgc_discovery_for_t_cell_immunology
    ```
    ##### _*If you are using Google Colab, you need to run `%cd emerald_bgcs_annotations` to change your current Colab directory to `emerald_bgcs_annotations`._    
     
  - To install miniconda, if you don't have anaconda or python already installed:
  
    ```
    $ wget https://repo.anaconda.com/miniconda/Miniconda3-py39_4.11.0-Linux-x86_64.sh    
    $ bash Miniconda3-py39_4.11.0-Linux-x86_64.sh
    $ source ~/.bashrc
    ```
    
    ##### _*On executing `bash Miniconda3-latest-Linux-x86_64.sh`, you need to type yes when requried. `source ~/.bashrc` will be required to add conda aliases and paths to your shell environment. If you are using Google Colab, you can skip installing miniconda._ 
            
  - To create bgcs conda environment:
  
    ```
    $ conda create -n bgcs python=3.7
    ```
    ##### _*If you are using Google Colab, you can skip creating bgcs environment._
     
  - To activate bgcs environment and install python packages:
  
    ```
    $ conda activate bgcs
    $ pip install -r requirements.txt
    ```
    
  - To install Prodigal for genes identificaton and translation
  
    ```
    $ conda install -c bioconda prodigal
    ```    
    
#### **Discovering BGCs in whole genome shotgun sequences**

  
  - Run ```BGCs DISCOVERY.ipynb``` on whole genome accessions to identify saccharide BGCs on microbial whole genome sequences.
  
  
#### **BGCs new training datasets**

  - This repository provides MiBiG BGCs training dataset ```data/training_dataset/mibig```, deepBGC non-bgcs dataset ```data/training_dataset/deepbgc/no_bgcs_seq_train_prodigal.csv``` and EMERALD new BGCs datasets ```data/training_dataset/emerald``` 
   

#### **Contact information** 

  - For help or issues using BGCs discovery repository, please contact Maaly Nassar (maaly13@yahoo.com). 
