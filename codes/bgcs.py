from pathlib import Path
import os
import pandas as pd
from gensim.models.fasttext import FastText
from sklearn.ensemble import RandomForestClassifier # random forest classifier
import joblib
import time


class bgcs:
    
    def __init__(self):
        pass
    
    def predict_classes(self,data,rf_model_path,features_type,vector_model_path,prob_lm,return_pred_text):

        ### loading features deep/machine learning models ###

        if features_type == 'tfidf':
            tfidf_vect = TfidfVectorizer(analyzer=clean_text)

        if features_type == 'fasttext':
            wvmodel = FastText.load(vector_model_path)

        if features_type == 'doc2vec':
            dvmodel = Doc2Vec.load(vector_model_path)

        ### loading random forest classifier ###

        [features,rf_model,X_test,Y_test] = joblib.load(rf_model_path) # random forest trained model


        ### generating texts features and classifying them with random forest classifier ###

        pred_df = pd.DataFrame()        
        splt = 1000
        start = list(range(0,len(data),splt))
        end = [*list(range(splt,len(data),splt)),*[len(data)]]

        for n,m in enumerate(start):
            start_time = time.time()

            ### features generation ###

            T_features = pd.DataFrame()            

            if features_type == 'tfidf':
                T_tfidf = tfidf_vect.fit_transform(data.iloc[m:end[n]]['text'].values.astype('U'))# values.astype('U') np.nan error
                T_features = pd.DataFrame(T_tfidf.toarray())
                T_features.columns = tfidf_vect.get_feature_names()
                T_features = T_features.reindex(features,axis=1,fill_value=0)

            if features_type in ['fasttext','doc2vec']:                
                for t,txt in enumerate(data.iloc[m:end[n]]['text']):
                    if features_type == 'fasttext':
                        txtv = wvmodel.wv[txt]                        
                    if features_type == 'doc2vec':
                        txtwords = [word.text for word in nlp(txt)]
                        txtv = dvmodel.infer_vector(txtwords)
                    vdf = pd.DataFrame(txtv).transpose() 
                    T_features = T_features.append(vdf)          

            elapsed_time = time.time() - start_time
            #print(features_type+':'+str(round(elapsed_time/60,3)))

            ### classes prediction ###

            YT_pred = {}
            YT_pred['id'] = data.iloc[m:end[n]]['id']
            if return_pred_text == 'yes': YT_pred['text'] = data.iloc[m:end[n]]['text']
            YT_pred['label'] = rf_model.predict(T_features)
            YT_pred['prob'] = list(rf_model.predict_proba(T_features))           
            YT_pred['prob'] = [round(probs[list(rf_model.classes_).index(YT_pred['label'][p])],3) for p,probs in enumerate(YT_pred['prob'])] 
            YT_pred_df = pd.DataFrame(YT_pred)
            YT_pred_df = YT_pred_df[YT_pred_df['prob']>prob_lm]
            YT_pred_df = YT_pred_df.sort_values(by=['prob'],ascending=False)

            pred_df = pred_df.append(YT_pred_df)            

            elapsed_time = time.time() - start_time
            #print('prediction:'+str(round(elapsed_time/60,3)))          

        pred_df = pred_df.sort_values(by=['prob'],ascending=False) 
        pred_df = pred_df.reset_index(drop=True)

        return pred_df
    
    
    def identify(self,accs_proteins_df,rf_model_path,features_type,vector_model_path,prob_lm,return_pred_text):
        
        bgc_pred_df = self.predict_classes(accs_proteins_df,rf_model_path,features_type,vector_model_path,prob_lm,return_pred_text) #gene docvec on gene
        bgc_pred_df = bgc_pred_df.sort_values(by='id').reset_index(drop=True)

        clust = []
        c = 0
        for r,row in bgc_pred_df.iterrows():
            if row['label'] == 'bgc':
                clust.append(c)
            else:
                c +=1
                clust.append('none')
        bgc_pred_df['cluster'] = clust
        bgc_pred_dfg = bgc_pred_df.groupby(['cluster']).agg({'id':list,'prob':list,'label':list}).reset_index(drop=True)
        bgc_pred_dfg['bgc_count'] = [len(lbls) for lbls in bgc_pred_dfg['label']]
        bgc_pred_dfg = bgc_pred_dfg.sort_values(by='bgc_count',ascending=False).reset_index(drop=True)
        
        return [bgc_pred_df,bgc_pred_dfg]
        
               
        
    def classify(self,bgc_pred_dfg, accs_proteins_df,rf_model_path,features_type,vector_model_path,prob_lm,return_pred_text):
        
        class_pred_dfg = pd.DataFrame()

        for r,row in bgc_pred_dfg.iterrows():
            clustdf = accs_proteins_df[accs_proteins_df['id'].isin(row['id'])]
            txt = '. '.join(clustdf['text'])
            clustdf = pd.DataFrame({'id':[row['id']],'text':[txt]})
            pred = self.predict_classes(clustdf,rf_model_path,features_type,vector_model_path,prob_lm,return_pred_text) #gene docvec on gene
            pred['bgc_label'] = [row['label']]
            pred['bgc_prob'] = [row['prob']]
            class_pred_dfg = class_pred_dfg.append(pred)
        
        class_pred_dfg = class_pred_dfg.reset_index(drop=True)
        
        return class_pred_dfg


