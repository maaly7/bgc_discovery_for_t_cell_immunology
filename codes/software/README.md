## **To install Prodigal and other BGC discovery tools** 

#### **Gene identification and translation tool: [PRODIGAL](https://github.com/hyattpd/Prodigal)**

  - To install Prodigal:
  
      - From GitHub repository resource
  
        ```
        $ git clone https://github.com/hyattpd/Prodigal.git
        $ cd Prodigal
        $ make install
        ```
      - From Bioconda
      
        ```
        $ conda install -c bioconda prodigal
        ```  
            
#### **EMBL-Heidelberg BGC discovery tool: [GECCO](https://gecco.embl.de/)**

  - included in requirements.txt
  
  
#### **EMBL-EBI BGC discovery tool: [emeraldBGC](https://github.com/Finn-Lab/emeraldBGC)**

  - To install emeraldBGC:
  
    ```
    $ conda install -c bioconda emeraldbgc
    ```  